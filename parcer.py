from bs4 import BeautifulSoup
import re
import requests


RU_PHONE_REGEXP = re.compile('^(\\+7|7|8)?[\\s\\-]?\\(?[489][0-9]{2}\\)?'
                             '[\\s\\-]?[0-9]{3}[\\s\\-]?[0-9]{2}[\\s\\-]?[0-9]{2}$')


def format_number(number: str) -> str:
    """
    Приведение номера к нужному формату
    :param number: Не отформатированный телефонный номер
    :return: Отформатированный телефонный номер
    """
    cleaned_number = re.sub(r'\D', '', number)
    if len(cleaned_number) == 7:
        return f'8{cleaned_number[1:]}'
    return cleaned_number


def extract_phones(web_page_url: str) -> list:
    """
    Извлечение телефонных номеров с веб страницы
    :param web_page_url: Веб страница с которой нужно извлечь телефонные номера
    :return: Список распознанных телефонных номеров
    """
    r = requests.get(web_page_url)
    bs = BeautifulSoup(r.text, features='lxml')
    numbers = []
    for number in bs.find_all(text=RU_PHONE_REGEXP):
        phone_number = format_number(number)
        if phone_number not in numbers:
            numbers.append(phone_number)
    return numbers


print(extract_phones('https://masterdel.ru'))
print(extract_phones('https://repetitors.info'))
